
package it.emate.corsi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableJpaAuditing
@EnableSwagger2
public class Boot {

//    @RequestMapping("/")
//    public String home() {
//        return "Ciao Domenico, io ho fatto";
//    }

    public static void main(String[] args) {
        SpringApplication.run(Boot.class, args);
    }

}
