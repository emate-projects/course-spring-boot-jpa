/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.emate.corsi.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pako8
 */
@Entity
@Table(name = "UTENTE")
@NamedQueries({
    @NamedQuery(name = "Utente.findAll", query = "SELECT u FROM Utente u")})
public class Utente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "uuid")
    private String uuid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Column(name = "counter_licenze")
    private int counterLicenze;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "utente")
    private Collection<AssModuloUtente> assModuloUtenteCollection;
    @JoinColumn(name = "ruolo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonIgnoreProperties(value = {"utenteCollection"}, allowSetters = true)
    private Ruolo ruolo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "utente")
    private Collection<AssUtenteAzienda> assUtenteAziendaCollection;

    public Utente() {
    }

    public Utente(String uuid) {
        this.uuid = uuid;
    }

    public Utente(String uuid, String email, int counterLicenze) {
        this.uuid = uuid;
        this.email = email;
        this.counterLicenze = counterLicenze;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    public int getCounterLicenze() {
        return counterLicenze;
    }

    public void setCounterLicenze(int counterLicenze) {
        this.counterLicenze = counterLicenze;
    }

    public Collection<AssModuloUtente> getAssModuloUtenteCollection() {
        return assModuloUtenteCollection;
    }

    public void setAssModuloUtenteCollection(Collection<AssModuloUtente> assModuloUtenteCollection) {
        this.assModuloUtenteCollection = assModuloUtenteCollection;
    }

    public Ruolo getRuolo() {
        return ruolo;
    }

    public void setRuolo(Ruolo ruolo) {
        this.ruolo = ruolo;
    }

    public Collection<AssUtenteAzienda> getAssUtenteAziendaCollection() {
        return assUtenteAziendaCollection;
    }

    public void setAssUtenteAziendaCollection(Collection<AssUtenteAzienda> assUtenteAziendaCollection) {
        this.assUtenteAziendaCollection = assUtenteAziendaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (uuid != null ? uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Utente)) {
            return false;
        }
        Utente other = (Utente) object;
        if ((this.uuid == null && other.uuid != null) || (this.uuid != null && !this.uuid.equals(other.uuid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.ethica.trattamenti.persistence.model.Utente[ uuid=" + uuid + " ]";
    }
    
}
