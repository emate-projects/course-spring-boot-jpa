/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.emate.corsi.persistence.util;

/**
 *
 * @author michele
 */
public class TransactionHandlingException extends Exception {

    private static final long serialVersionUID = 8935949903332003499L;
    private static final String MESSAGE="Transaction Handling Error";

    public TransactionHandlingException(Throwable cause) {
        super(TransactionHandlingException.MESSAGE+" : ", cause);

    }

    public TransactionHandlingException(String message, Throwable cause) {
        super(TransactionHandlingException.MESSAGE+" : "+message, cause);

    }

}
