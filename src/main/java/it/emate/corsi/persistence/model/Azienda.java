/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.emate.corsi.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pako8
 */
@Entity
@Table(name = "AZIENDA")
@NamedQueries({
    @NamedQuery(name = "Azienda.findAll", query = "SELECT a FROM Azienda a")})
public class Azienda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "denominazione")
    private String denominazione;
    @Size(max = 255)
    @Column(name = "cod_fiscale")
    private String codFiscale;
    @Size(max = 75)
    @Column(name = "uuid_ins")
    private String uuidIns;
    @Size(max = 75)
    @Column(name = "uuid_mod")
    private String uuidMod;
    @Size(max = 255)
    @Column(name = "contitolare")
    private String contitolare;
    @Column(name = "dt_ins")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtIns;
    @Column(name = "dt_mod")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtMod;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "email")
    private String email;
    @Size(max = 255)
    @Column(name = "forma_giur")
    private String formaGiur;
    @Size(max = 255)
    @Column(name = "ins_sede_leg")
    private String insSedeLeg;
    @Size(max = 255)
    @Column(name = "pec")
    private String pec;
    @Size(max = 255)
    @Column(name = "piva")
    private String piva;
    @Size(max = 255)
    @Column(name = "telefono")
    private String telefono;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "azienda")
    @JsonIgnoreProperties({"azienda"})
    private Collection<AssUtenteAzienda> assUtenteAziendaCollection;

    public Azienda() {
    }

    public Azienda(Integer id) {
        this.id = id;
    }

    public Azienda(Integer id, String denominazione) {
        this.id = id;
        this.denominazione = denominazione;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDenominazione() {
        return denominazione;
    }

    public void setDenominazione(String denominazione) {
        this.denominazione = denominazione;
    }

    public String getCodFiscale() {
        return codFiscale;
    }

    public void setCodFiscale(String codFiscale) {
        this.codFiscale = codFiscale;
    }

    public String getUuidIns() {
        return uuidIns;
    }

    public void setUuidIns(String uuidIns) {
        this.uuidIns = uuidIns;
    }

    public String getUuidMod() {
        return uuidMod;
    }

    public void setUuidMod(String uuidMod) {
        this.uuidMod = uuidMod;
    }

    public String getContitolare() {
        return contitolare;
    }

    public void setContitolare(String contitolare) {
        this.contitolare = contitolare;
    }

    public Date getDtIns() {
        return dtIns;
    }

    public void setDtIns(Date dtIns) {
        this.dtIns = dtIns;
    }

    public Date getDtMod() {
        return dtMod;
    }

    public void setDtMod(Date dtMod) {
        this.dtMod = dtMod;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFormaGiur() {
        return formaGiur;
    }

    public void setFormaGiur(String formaGiur) {
        this.formaGiur = formaGiur;
    }

    public String getInsSedeLeg() {
        return insSedeLeg;
    }

    public void setInsSedeLeg(String insSedeLeg) {
        this.insSedeLeg = insSedeLeg;
    }

    public String getPec() {
        return pec;
    }

    public void setPec(String pec) {
        this.pec = pec;
    }

    public String getPiva() {
        return piva;
    }

    public void setPiva(String piva) {
        this.piva = piva;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Collection<AssUtenteAzienda> getAssUtenteAziendaCollection() {
        return assUtenteAziendaCollection;
    }

    public void setAssUtenteAziendaCollection(Collection<AssUtenteAzienda> assUtenteAziendaCollection) {
        this.assUtenteAziendaCollection = assUtenteAziendaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Azienda)) {
            return false;
        }
        Azienda other = (Azienda) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.ethica.trattamenti.persistence.model.Azienda[ id=" + id + " ]";
    }
    
}
