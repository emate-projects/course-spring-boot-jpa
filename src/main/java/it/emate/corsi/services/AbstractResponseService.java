package it.emate.corsi.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author cg07090
 *
 */
public abstract class AbstractResponseService {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	protected String accessToken;

	/**
	 * 
	 * @param input
	 *            Può essere nullo se la RequestBody non è prevista
	 * @param tabella
	 * @param orgcode
	 */
//	abstract void inputManager(InputService input, String tabella, String orgcode) throws InvalidRequestException;

	/**
	 * 
	 * @param response
	 * @return
	 */
//	abstract Response outputManager(Response response);

	/**
	 * 
	 * @return
	 */
//	abstract Response accessNotAllowed() throws InvalidRequestException;

	/**
	 * 
	 * @param input
	 * @return
	 */
//	abstract Response logicInsertOneExecute(InputService input, String tabella, String orgcode) throws DaoException;

	/**
	 * 
	 * @param input
	 * @return
	 */
//	abstract Response logicUpdateOneExecute(InputService input, String tabella, String orgcode) throws DaoException;

	/**
	 * 
	 * @param input
	 * @return
	 */
//	abstract Response logicDeleteOneExecute(InputService input, String tabella, String orgcode) throws DaoException;

	/**
	 * 
	 * @return
	 */
//	abstract Response logicSelectAllExecute(String tabella, String orgcode) throws DaoException;

	/**
	 * 
	 * @param input
	 * @return
	 */
//	abstract Response logicSelectOneExecute(InputService input, String tabella, String orgcode) throws DaoException;
}
