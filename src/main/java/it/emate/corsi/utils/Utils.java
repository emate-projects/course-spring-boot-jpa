package it.emate.corsi.utils;

import java.util.ArrayList;
import java.util.List;

//import it.ethica.trattamenti.persistence.model.RegTrat;

/**
 * This classes collects all utility methods
 *
 */
public class Utils {
	
	/**
	 * This function build a list with the 
	 * user uuid and a user to null, for searching purposes.
	 * @param uuid
	 * @return userListWithNull
	 */
	public static List<String> tipologyUserListWithNull (String uuid){
		List<String> userListWithNull = new ArrayList<>();
		if (uuid!=null) {
			userListWithNull.add(uuid);
		}
		userListWithNull.add("");
	  return userListWithNull;
	}
	
//	/**
//	 * method for building a correct entity regtrat for update and save
//	 * @param regtratTo
//	 * @param regtratFrom
//	 * @return regtratTo
//	 */
//	public static RegTrat settingDbregtrat (RegTrat regtratTo, RegTrat regtratFrom) {
//		regtratTo.setCodUtenteIns(regtratFrom.getCodUtenteIns());
//		regtratTo.setCodUtenteMod(regtratFrom.getCodUtenteMod());
//		regtratTo.setDataFinalizzazione(regtratFrom.getDataFinalizzazione());
//		regtratTo.setDatiDpo(regtratFrom.getDatiDpo());
//		regtratTo.setDatiRappTit(regtratFrom.getDatiRappTit());
//		regtratTo.setDatiTitTrat(regtratFrom.getDatiTitTrat());
//		regtratTo.setDtIns(regtratFrom.getDtIns());
//		regtratTo.setDtMod(regtratFrom.getDtMod());
//		regtratTo.setFlagAccTit(regtratFrom.getFlagAccTit());
//		regtratTo.setFlagAnalisiRso(regtratFrom.getFlagAnalisiRso());
//		regtratTo.setFlagConsenso(regtratFrom.getFlagConsenso());
//		regtratTo.setFlagFinalizzato(regtratFrom.getFlagFinalizzato());
//		regtratTo.setFlagInformativa(regtratFrom.getFlagInformativa());
//		regtratTo.setFlagLargaScala(regtratFrom.getFlagLargaScala());
//		regtratTo.setFlagLettInc(regtratFrom.getFlagLettInc());
//		regtratTo.setFlagProcBackup(regtratFrom.getFlagProcBackup());
//		regtratTo.setFlagTrasfEstero(regtratFrom.getFlagTrasfEstero());
//		regtratTo.setFlagValutazione(regtratFrom.getFlagValutazione());
//		regtratTo.setFormatoDati(regtratFrom.getFormatoDati());
//		regtratTo.setMisArchCartaceo(regtratFrom.getMisArchCartaceo());
//		regtratTo.setMisArchElettr(regtratFrom.getMisArchElettr());
//		regtratTo.setMisSicBackup(regtratFrom.getMisSicBackup());
//		regtratTo.setNomeContitolare(regtratFrom.getNomeContitolare());
//		regtratTo.setNomeRespEsterni(regtratFrom.getNomeRespEsterni());
//		regtratTo.setPaeseOrg(regtratFrom.getPaeseOrg());
//		regtratTo.setPeriodoConservazione(regtratFrom.getPeriodoConservazione());
//		regtratTo.setProcCancDati(regtratFrom.getProcCancDati());
//		regtratTo.setTplReg(regtratFrom.getTplReg());
//		regtratTo.setTrattamento(regtratFrom.getTrattamento());
//		regtratTo.setUbicazioneBackup(regtratFrom.getUbicazioneBackup());
//		regtratTo.setUbicazioneFormCartaceo(regtratFrom.getUbicazioneFormCartaceo());
//		regtratTo.setUbicazioneFormElettr(regtratFrom.getUbicazioneFormElettr());
//		regtratTo.setUtenteFinalizzazione(regtratFrom.getUtenteFinalizzazione());
//		regtratTo.setUuid(regtratFrom.getUuid());
//		
//		
//		
//		
//		//AssRegArt6Collection
//		if (regtratFrom.getAssRegArt6Collection()!=null) {
//			if (regtratTo.getAssRegArt6Collection()!=null) {
//				regtratTo.getAssRegArt6Collection().clear();
//				regtratTo.getAssRegArt6Collection().addAll(regtratFrom.getAssRegArt6Collection());
//			}else {
//				regtratTo.setAssRegArt6Collection(regtratFrom.getAssRegArt6Collection());
//			}
//		}
//		//AssRegArt9Collection
//		if (regtratFrom.getAssRegArt9Collection()!=null) {
//			if (regtratTo.getAssRegArt9Collection()!=null) {
//				regtratTo.getAssRegArt9Collection().clear();
//				regtratTo.getAssRegArt9Collection().addAll(regtratFrom.getAssRegArt9Collection());
//			} else {
//				regtratTo.setAssRegArt9Collection(regtratFrom.getAssRegArt9Collection());
//			}
//		}
//		//AssRegCatDatoCollection
//		if (regtratFrom.getAssRegCatDatoCollection()!=null) {
//			if (regtratTo.getAssRegCatDatoCollection()!=null) {
//				regtratTo.getAssRegCatDatoCollection().clear();
//				regtratTo.getAssRegCatDatoCollection().addAll(regtratFrom.getAssRegCatDatoCollection());
//			} else {
//				regtratTo.setAssRegCatDatoCollection(regtratFrom.getAssRegCatDatoCollection());
//			}
//		}
//		//AssRegCatDestCollection
//		if (regtratFrom.getAssRegCatDestCollection()!=null) {
//			if (regtratTo.getAssRegCatDestCollection()!=null) {
//				regtratTo.getAssRegCatDestCollection().clear();
//				regtratTo.getAssRegCatDestCollection().addAll(regtratFrom.getAssRegCatDestCollection());
//			} else {
//				regtratTo.setAssRegCatDestCollection(regtratFrom.getAssRegCatDestCollection());
//			}
//		}
//		//AssRegCatIntCollection
//		if (regtratFrom.getAssRegCatIntCollection()!=null) {
//			if (regtratTo.getAssRegCatIntCollection()!=null) {
//				regtratTo.getAssRegCatIntCollection().clear();
//				regtratTo.getAssRegCatIntCollection().addAll(regtratFrom.getAssRegCatIntCollection());
//			} else {
//				regtratTo.setAssRegCatIntCollection(regtratFrom.getAssRegCatIntCollection());
//			}
//		}
//		//AssRegFinTratCollection
//		if (regtratFrom.getAssRegFinTratCollection()!=null) {
//			if (regtratTo.getAssRegFinTratCollection()!=null) {
//				regtratTo.getAssRegFinTratCollection().clear();
//				regtratTo.getAssRegFinTratCollection().addAll(regtratFrom.getAssRegFinTratCollection());
//			} else {
//				regtratTo.setAssRegFinTratCollection(regtratFrom.getAssRegFinTratCollection());
//			}
//		}
//		if (regtratFrom.getAssRegFonteCollection()!=null) {
//			if (regtratTo.getAssRegFonteCollection()!=null) {
//				regtratTo.getAssRegFonteCollection().clear();
//				regtratTo.getAssRegFonteCollection().addAll(regtratFrom.getAssRegFonteCollection());
//			} else {
//				regtratTo.setAssRegFonteCollection(regtratFrom.getAssRegFonteCollection());
//			}
//		}
//		//AssRegGarTrasfCollection
//		if (regtratFrom.getAssRegGarTrasfCollection()!=null) {
//			if (regtratTo.getAssRegGarTrasfCollection()!=null) {
//				regtratTo.getAssRegGarTrasfCollection().clear();
//				regtratTo.getAssRegGarTrasfCollection().addAll(regtratFrom.getAssRegGarTrasfCollection());
//			} else {
//				regtratTo.setAssRegGarTrasfCollection(regtratFrom.getAssRegGarTrasfCollection());
//			}
//		}
//		//AssRegOrgIntCollection
//		if (regtratFrom.getAssRegOrgIntCollection()!=null) {
//			if (regtratTo.getAssRegOrgIntCollection()!=null) {
//				regtratTo.getAssRegOrgIntCollection().clear();
//				regtratTo.getAssRegOrgIntCollection().addAll(regtratFrom.getAssRegOrgIntCollection());
//			} else {
//				regtratTo.setAssRegOrgIntCollection(regtratFrom.getAssRegOrgIntCollection());
//			}
//		}
//		//AssRegPaesiEstCollection
//		if (regtratFrom.getAssRegPaesiEstCollection()!=null) {
//			if (regtratTo.getAssRegPaesiEstCollection()!=null) {
//				regtratTo.getAssRegPaesiEstCollection().clear();
//				regtratTo.getAssRegPaesiEstCollection().addAll(regtratFrom.getAssRegPaesiEstCollection());
//			} else {
//				regtratTo.setAssRegPaesiEstCollection(regtratFrom.getAssRegPaesiEstCollection());
//			}
//		}
//		//AssRegRagConsCollection
//		if (regtratFrom.getAssRegRagConsCollection()!=null) {
//			if (regtratTo.getAssRegRagConsCollection()!=null) {
//				regtratTo.getAssRegRagConsCollection().clear();
//				regtratTo.getAssRegRagConsCollection().addAll(regtratFrom.getAssRegRagConsCollection());
//			} else {
//				regtratTo.setAssRegRagConsCollection(regtratFrom.getAssRegRagConsCollection());
//			}
//		}
//		//AssRegTpCollection
//		if (regtratFrom.getAssRegTpCollection()!=null) {
//			if (regtratTo.getAssRegTpCollection()!=null) {
//				regtratTo.getAssRegTpCollection().clear();
//				regtratTo.getAssRegTpCollection().addAll(regtratFrom.getAssRegTpCollection());
//			} else {
//				regtratTo.setAssRegTpCollection(regtratFrom.getAssRegTpCollection());
//			}
//		}
//		//AssRegTplDatoCollection
//		if (regtratFrom.getAssRegTplDatoCollection()!=null) {
//			if (regtratTo.getAssRegTplDatoCollection()!=null) {
//				regtratTo.getAssRegTplDatoCollection().clear();
//				regtratTo.getAssRegTplDatoCollection().addAll(regtratFrom.getAssRegTplDatoCollection());
//			} else {
//				regtratTo.setAssRegTplDatoCollection(regtratFrom.getAssRegTplDatoCollection());
//			}
//		}
//		return regtratTo;
//		
//	}

}

 
