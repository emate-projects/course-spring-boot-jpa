/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.emate.corsi.persistence.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author pako8
 */
@Entity
@Table(name = "ASS_UTENTE_AZIENDA")
@NamedQueries({
    @NamedQuery(name = "AssUtenteAzienda.findAll", query = "SELECT a FROM AssUtenteAzienda a")})
public class AssUtenteAzienda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "utente", referencedColumnName = "uuid")
    @ManyToOne(optional = false)
    @JsonIgnoreProperties(value = {"assUtenteAziendaCollection", "assModuloUtenteCollection"}, allowSetters = true)
    private Utente utente;
    @JoinColumn(name = "azienda", referencedColumnName = "id")
    @ManyToOne(optional = false)
    @JsonIgnoreProperties(value = {"assUtenteAziendaCollection"}, allowSetters = true)
    private Azienda azienda;

    public AssUtenteAzienda() {
    }

    public AssUtenteAzienda(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public Azienda getAzienda() {
        return azienda;
    }

    public void setAzienda(Azienda azienda) {
        this.azienda = azienda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssUtenteAzienda)) {
            return false;
        }
        AssUtenteAzienda other = (AssUtenteAzienda) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "it.ethica.trattamenti.persistence.model.AssUtenteAzienda[ id=" + id + " ]";
    }
    
}
