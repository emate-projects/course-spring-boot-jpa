/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.emate.corsi.services;

import it.emate.corsi.controllers.UtenteController;
import it.emate.corsi.persistence.model.Utente;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author pako8
 */
@RestController
@RequestMapping("/utente")
public class UtenteService{
    
        
    @Autowired
    UtenteController utenteController;
    
    protected final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass());
    
     /**
     * Returns all utente data
     * @param accessToken
     * @return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/readAll", produces = { "application/json" })
    public List<Utente> readAll() {
        log.debug("***** Eseguo selectAll INIZIO***** ");
        
        try {

            return utenteController.getAllUtente();
                
        } catch (Exception e) {
           
            Logger.getLogger(UtenteService.class.getName()).log(Level.SEVERE, null, e);
            return null;
        } 
    }
    
    
    /**
     * Insert or update of new Utente
     * @param utente
     * @return response
     */
    @RequestMapping(method = RequestMethod.POST, value = "/save", produces = { "application/json" },
    		        consumes = { "application/json" })
    public boolean save(@RequestBody(required = true) final Utente utente) {
        log.debug("***** Eseguo insert Regtrat INIZIO***** ");

        try {

              utenteController.insertUpdateUtente(utente);
              return true;

        } catch (Exception e) {

            return false;
        } 
    }
    
    /**
     * Insert or update of new Utente
     * @param codiceUtente
     * @return response
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/delete",  produces = { "application/json" })
    public boolean delete(@RequestParam(value = "codiceUtente", required = true) final int  codiceUtente) {
        log.debug("***** Eseguo delete Regtrat INIZIO***** ");
        
        try {

            utenteController.deleteUtente(codiceUtente);
            return true;

        } catch (Exception e) {

            Logger.getLogger(UtenteService.class.getName()).log(Level.SEVERE, null, e);
            return false;
        } 
    }
}
