/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.emate.corsi.persistence.dao;

import it.emate.corsi.persistence.model.Ruolo;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author pako8
 */
@Repository
@Transactional
public interface RuoloDAO extends JpaRepository<Ruolo, Integer> {
    
}
