/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.emate.corsi.persistence.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author michele
 */
public class JPAUtil {

    private static EntityManagerFactory entityManagerFactory = null;
    private static InheritableThreadLocal<EntityManager> entityManagerThreadLocal = new InheritableThreadLocal<>();


    public static void init(String persistanceUnitName){

        try {

            entityManagerFactory = Persistence.createEntityManagerFactory(persistanceUnitName);

        } catch (Throwable ex) {

                // Make sure you log the exception, as it might be swallowed
                // ---- ---- --- --- --- ---------- -- -- ----- -- ---------
                System.err.println("Initial EntityManagerFactory creation failed." + ex);
                throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * Get the configured entityManagerFactory
     *
     * @return entityManagerFactory
     */
    public static EntityManagerFactory getEntityManagerFactory() {

        if(entityManagerFactory==null){

            try {

                entityManagerFactory = Persistence.createEntityManagerFactory("ethica-registro-trattamenti");

            } catch (Throwable ex) {

                // Make sure you log the exception, as it might be swallowed
                // ---- ---- --- --- --- ---------- -- -- ----- -- ---------
                System.err.println("Initial EntityManagerFactory creation failed." + ex);
                throw new ExceptionInInitializerError(ex);
            }
        }
        return entityManagerFactory;
    }

    /**
     * Get entity manager from thread
     * 
     * @return entity manager
     */
    public static EntityManager getEntityManager() {

        getEntityManagerFactory();

        if (entityManagerThreadLocal.get() == null || entityManagerThreadLocal.get().isOpen() == false) {

            entityManagerThreadLocal.set(entityManagerFactory.createEntityManager());
        }
        return entityManagerThreadLocal.get();
    }
}
