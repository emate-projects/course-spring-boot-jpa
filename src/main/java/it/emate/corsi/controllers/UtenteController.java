/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.emate.corsi.controllers;

import it.emate.corsi.persistence.dao.UtenteDAO;
import it.emate.corsi.persistence.model.Utente;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Controller;

/**
 *
 * @author pako8
 */
@Controller
public class UtenteController {
    
    
    @Autowired
    UtenteDAO utenteDao;
   
    /**
     * Find all Utente
     * @return listaUtente
     * @throws Exception
     */
    public  List<Utente> getAllUtente() throws Exception {
        
       List< Utente> listaUtente = utenteDao.findAll();
        
       return listaUtente;
    }
    
    /**
     * Find one utente by id
     * @return listaUtente
     * @throws Exception
     */
    public  Utente getUtenteByUuid(String uuid) throws Exception {
        
        Utente utente = utenteDao.findByUuid(uuid);
        
        if (utente != null)
            return utente;
        else
            return new Utente();

    }
    
    /**
     * Find one utente by uuid
     * @return listaUtente
     * @throws Exception
     */
//    public  List<Utente> getUtenteByCliente(String cliente) throws Exception {
//        
//    	List<Utente> listUtente =  utenteDao.findByCliente(cliente);
//        
//       return listUtente;
//    }
    
    /**
     * Insert one utente 
     * @return listaUtente
     * @throws Exception
     */
    public void insertUpdateUtente(Utente utente) throws Exception {
    	Utente utenteOrig = null;
    	if (utente.getUuid()!=null) {
    		 utenteOrig = utenteDao.findByUuid(utente.getUuid());
    	} else {
    		 utenteOrig = new Utente();
    	}
//    	utenteOrig = Utils.settingDbUtente(utenteOrig, utente);
    	this.utenteDao.saveAndFlush(utenteOrig);
        
    }
    
    /**
     * Delete one utente 
     * @return listaUtente
     * @throws Exception
     */
    public boolean deleteUtente(int codiceUtente) throws Exception {
    	 boolean success = false;
    	 Utente utentedel = utenteDao.findById(codiceUtente).orElse(null);
    	 if (utentedel != null) {
    		 utenteDao.delete(utentedel);
    	 }else {
    		 throw new ChangeSetPersister.NotFoundException();
    	 }
        return success;
    }
    
        
}
